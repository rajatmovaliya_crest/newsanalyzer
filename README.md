# EventAnalyzer

    Show relavent events based on user's interest in event graph.

## Setup

* Setup Mongodb as a database
    - Download from this [link](https://www.mongodb.com/dr/fastdl.mongodb.org/win32/mongodb-win32-x86_64-2012plus-4.2.2-signed.msi/download)
* Run `python news_analyzer.py` at root of this project
* It will start flask server at given constants (htttps://HOST:PORT) defined in news_analyzer

#### NLTK Libraries Requirement
```
import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
```

## Want to Contribute ?

#### Follow the steps:

##### To setup ssh key, if done already then skip it

    * Windows
      * ssh-keygen
      * press [Enter] until created.
      * `eval $(ssh-agent)`
      * ssh-add <path/to/private/ssh/key/id_rsa>
      * Add "path/to/public/ssh/key/user/.ssh/id_rsa.pub" to Bitbucket "Settings -> Access Keys -> Add Key"

##### Cloning and Creating PR:

* `git clone https://rajatmovaliya_crest@bitbucket.org/rajatmovaliya_crest/newsanalyzer.git; cd newsanalyzer`
* `git checkout -b feat-<name-of-your-feature>`

* Note: If you are working long time locally on your branch, the remote master branch may get ahead,
So do following step often:
    * `git pull master`
    * `git checkout feat-<name-of-your-feature>`
    * `git rebase master`

* Once your feature is complete,

    * `git push origin feat-<name-of-your-feature>`
* Create PR on bit-bucket/github
* Add reviewer
* Commit changes requested and push branch again, repeat this step until PR is ready to merge to master
* Before merging PR, do rebase one more time to ensure in history, your branch is created from top of the master.
* Merge PR

# Authors

* Rajat Movaliya
* Dishank Khatri
* Meetrajsinh Solanki


