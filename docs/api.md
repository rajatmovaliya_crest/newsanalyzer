# API Documentation

### Endpoints

* URL: /events/<event_id>?limit=<integer>

    e.g /events/1?limit=2

    Query Parameters:
        - limit (integer):
            - return only <limit> number of results.
            - Default value is 10

* URL: /events/<event_id>?fields=<field1_name>,<field_name2>,...

    e.g /events/1?fields=id,name

    Query Parameters:
        - fields (list of strings):
            - It will return response object with only given parameters and others will be skipped
            - Default will be all parameters

* /events?q=<query_to_search>

    e.g /events?q=<query_to_search>&fields=id,name

    Query Parameters:
        - q (string):
            - It will try to search all events matching to given query

* /events/<id>:

    * Parameters:
        - id: id of event

    * Response
        - Standard structure as defined in `response_sample/events.json` in this directory

* /events?name=Election&limit=1:

    * Query Parameters:
        - name: To return events that are matching with given name

    * Response
        - Standard structure as defined in `response_sample/event.json` in this directory

* /struct-events:
    * Response
        - Standard structure as defined in `response_sample/struct-event.json` with centre event as trending event.

* /struct-events/1:
    * Parameters:
      - id:
          - It is id of central event-news
    * Response:
      - Standard structure as defined in `response_sample/event.json` in this directory

* /struct-events/1:
    * Parameters:
      - name:
          - It is id of central event-news with given name
    * Response:
      - Standard structure as defined in `response_sample/struct-events.json` in this directory
