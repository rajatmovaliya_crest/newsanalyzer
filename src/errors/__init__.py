from http import HTTPStatus

class CustomException(Exception):
    ''' To separate built-in error and custom one '''
    pass
class EventIdNotFound(CustomException):

    def __init__(self, _id):
        self.msg = 'Event of given id not found'
        self.status = HTTPStatus.NOT_FOUND
        self.payload = {
            '_id': _id,
        }

class EventNameNotFound(CustomException):

    def __init__(self, name):
        self.msg = 'Event of given name not found'
        self.status = HTTPStatus.NOT_FOUND
        self.payload = {
            'name': name,
        }