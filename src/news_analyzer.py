import utils
import utils.db as db
import utils.daemon as daemon
import utils.trimmer as trimmer

from flask import Flask, request, render_template, jsonify
from errors import CustomException, EventIdNotFound, EventNameNotFound

HOST = '127.0.0.1'
PORT = 5000

app = Flask(__name__)

db = db.DataBase()

# start background services
daemon.start_bgs()

# Standard Responses
RESPS = {
    'event': {'event': {}, 'status': 200},
    'events': {'events': [], 'status': 200},
    'struct-events': {'struct-events': {'main': {}, 'related': []}, 'status': 200},
}

@app.route('/')
@app.route('/index')
def main():
    return render_template('index.html')

@app.route('/events/<_id>', methods=['GET'])
def get_events_by_id(_id):
    ''' return event of given id '''

    fields = request.args.get('fields')
    if isinstance(fields, str): fields = fields.split(',')
    event = db.get_event(_id=_id)

    response = RESPS['event']
    if fields: event = trimmer.trim_dict(event, fields)
    response['event'] = event

    if response['event'] == None: raise EventIdNotFound(_id)

    return jsonify(**response)

@app.route('/events', methods=['GET'])
def get_events():
    ''' return structured events of given name event '''

    limit = request.args.get('limit')
    fields = request.args.get('fields')
    if isinstance(fields, str): fields = fields.split(',')

    name = request.args.get('name')
    query = request.args.get('q')

    response = {}
    if name:
        response = RESPS['event']
        event = db.get_event(name=name)
        if event == None: raise EventNameNotFound(name)

        daemon.ids_queue.put((event['_id'],))
        if fields: event = trimmer.trim_dict(event, fields)
        response['event'] = event

    elif query:
        response = RESPS['events']
        events = db.get_events(query=query)
        if fields: events = trimmer.trim_dicts(events, fields)
        response['events'] = events

    else:
        response['events'] = []

    return jsonify(**response)

@app.route('/struct-events/<_id>', methods=['GET'])
def get_struct_events_by_id(_id):
    ''' return structured events of given id '''

    fields = request.args.get('fields')
    if isinstance(fields, str): fields = fields.split(',')
    limit = request.args.get('limit')

    main, related = db.get_struct_events(_id=_id)
    if main == None: raise EventIdNotFound(_id)

    daemon.ids_queue.put((main['_id'],))
    response = RESPS['struct-events']
    response['struct-events']['main'] = main
    response['struct-events']['related'] = related

    return jsonify(**response)

@app.route('/struct-events', methods=['GET'])
def get_struct_events():
    ''' return structured events of given id '''

    fields = request.args.get('fields')
    if isinstance(fields, str): fields = fields.split(',')
    limit = request.args.get('limit')
    name = request.args.get('name')

    response = {}
    if name:
        main, related = db.get_struct_events(name=name)
        if main == None: raise EventNameNotFound(name)

        daemon.ids_queue.put((main['_id'],))
        response = RESPS['struct-events']
        response['struct-events']['main'] = main
        response['struct-events']['related'] = related
    else:
        # show initial struct-events
        main, related = db.get_trending_struct_events()
        if main == None: raise EventIdNotFound(utils.TRENDING_EVENT_ID)

        response = RESPS['struct-events']
        response['struct-events']['main'] = main
        response['struct-events']['related'] = related

    return jsonify(**response)

# --------------------------------------------------------------------#
# All Error Handling Related Code

@app.errorhandler(CustomException)
def handle_error(error):
    ''' Catch all event id not found errors '''

    res = {
        'status': error.status,
        'message': error.msg,
        'payload': error.payload,
    }
    return jsonify(**res), error.status

if __name__ == "__main__":
    app.run(host=HOST, port=PORT, debug=True)