$(document).ready(function(){

    //CSS Property: top for formating Components
    var top=["0vh", "0vh", "35vh", "75vh" , "75vh", "35vh"];

    //CSS Property: left for formating Components
    var left=["12vh", "54vh", "78vh", "54vh", "12vh", "-10vh"];

    //Array contains visited News by User
    var previous = {};

    var container = $("#component-container");

    // Data which comes from API
    var api_data = [];

    var shifting = {
        "0" : {
            top : '0vh',
            left : '12vh'
        },
        "1" : {
            top : '0vh',
            left : '54vh'
        },
        "2" : {
            top : '35vh',
            left : '78vh'
        },
        "3" : {
            top : '75vh',
            left : '54vh'
        },
        "4" : {
            top : '75vh',
            left : '12vh'
        },
        "5" : {
            top : '35vh',
            left : '-10vh',
        },
    };

    $.ajax({
        url : '/struct-events',  // http://127.0.0.1/
        method : 'GET',
        success: function(response_data){
            api_data = response_data;
            // alert(response_data[0]);
            //Initialize First Data in Main Component
            creating_main_component(response_data["struct-events"]["main"]);

            //Initially set the Data According Search
            addComponent(response_data["struct-events"]["main"]["related_event_ids"], -1);
        },
        error: function(e){
            alert("URL is not working....");
        }
    });

    function convert_date(date){
        var today = new Date(date);
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        } 
        if (mm < 10) {
            mm = '0' + mm;
        } 
        var today = dd + '-' + mm + '-' + yyyy;
        return today;
    }



    function creating_main_component(element){
        console.log("Main Event Id: " + element['_id'] + "\n"); // Print Main Id for debugging

        $(".graph").children("#main-component").remove();
        var div = "<div id='main-component' title='"+element['_id']+"'>\n";
        if(element["image_url"]!=null)
            div += "<div class='main-photo' style='background-image: url("+element["image_url"].replace('"',"")+"');></div>\n";
        else
            div += "<div class='main-photo' style='background-image: url(/static/images/"+(Math.floor(Math.random() * 4) + 1)+".jpg');></div>\n";
        div += "<div class='news-content'>\n";
        div += "<h2 style='display:none;'>"+element["name"]+"</h2>";
        div += "<strong><p>"+element["title"]+"</p></strong>";
        div += "<p>"+element["description"]+"</p>\n";
        div += "</div>\n";
        div += "<div class='main-footer'>\n";
        div += "<span class='pull-left'>"+element["source"]["name"]+"</span>\n";
        if(element["published_at"]!=null)    
            div += "<span class='pull-right'>"+convert_date(element["published_at"])+"</span>\n";
        else
            div += "<span class='pull-right'>-</span>\n";
        div += "</div>";

        $(".graph").prepend(div);

        $(".news-content").slimScroll();
        $(".news-content").css("height","180px");
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }


    //Use to Add Components near to Main Component
    function addComponent(componentData, position){


        for(i=0; i<componentData.length; i++){
            if( position!=i){
                var componentClass = "component-" + i
                element = search(componentData[i]);
                var div_open = "<div class='component "+componentClass+"' title='"+componentData[i]+"'>";
                if(element["image_url"]!=null)
                    div_open += "<div class='component-photo' style='background-image:url("+element["image_url"]+");'></div>";
                else
                    div_open += "<div class='component-photo' style='background-image:url(/static/images/"+(Math.floor(Math.random() * 4) + 1)+".jpg');></div>\n";
                var name = element["name"].trim();
                var maxLength = 20;
                if(name.length >= maxLength){
                    name = name.substr(0,maxLength-2) + "...";
                }
                var child = "\t<div class='component-headline'><h4>" + name[0].toUpperCase() + name.substr(1,name.length+1) + "</h4></div>\n";
                var div_close = "</div>";
                var div = div_open + child + div_close;
                container.append(div);
                $(".component-"+i).css("top",top[i]);
                $(".component-"+i).css("left",left[i]);
            }
        }
    }

    var availableTags = [];
    var new_tags = {};


    $("#search-textbox").keyup(function(){
        var keyword = $(this).val();
        if(keyword.length>1){

            $.ajax({
                url : '/events?q='+keyword+'&fields=name,id',
                type : 'GET',
                success : function(data){
                    availableTags = [];
                    for(element in data["events"]){
                        availableTags.push(element["name"]);
                    }
                    
                }
            });

            $( "#search-textbox" ).autocomplete({
                source: availableTags
            });
        }
    });

    $( "#search-textbox" ).autocomplete({
        source: availableTags
    });

    $("#search").click(function(){
        // alert(new_tags["cancer"]);
        var query = $("#search-textbox").val();
        // alert(query);
        // var id = new_tags[$("#search-textbox").val()];
    
        $.ajax({
            url : '/struct-events?name='+query,  // http://127.0.0.1/
            method : 'GET',
            success: function(response_data){
                $("#error").hide();
                if(response_data["status"]==200){
                    $("#main-component").empty();
                    container.empty();
                    api_data = response_data;
                    //Initialize First Data in Main Component
                    creating_main_component(response_data["struct-events"]["main"]);

                    //Initially set the Data According Search
                    addComponent(response_data["struct-events"]["main"]["related_event_ids"], -1);
                }
                else{
                    alert("Keyword not found");
                }
            },  
            error: function(e){
                $("#error").show();
            }
        });
    });

    //Finding News by Id
    function search(id){
        for(element in api_data["struct-events"]["related"]){
            if(api_data["struct-events"]["related"][element]["_id"] == id)
                return api_data["struct-events"]["related"][element];
        }
        if (api_data["struct-events"]["main"]["_id"] == id)
            return api_data["struct-events"]["main"];
    }

    function find_position(element, id){
        for(i=0; i<element["related_event_ids"].length; i++){
            if(element["related_event_ids"][i]==id)
                return i;
        }
        return 0;
    }


    //When User wants to checkout other News
    $(document).on("click",".component",function(){

        //Selected Component Details
        var selectedComponent = $(this);
        var selectedCompId = $(this).attr("title");
        var selectedHealdine = selectedComponent.children("h4").text();
        var selectedElement = search(selectedCompId);
        var selectedClass = $(this).attr("class").split(" ")[1].split("-")[1];
        var selectedCompClass = $(this).attr("class").split(" ")[1].split("-")[1];
        
    
        $(".component").each(function(){
            var classId = $(this).attr("class").split(" ")[1].split("-")[1];
            if(classId!=selectedCompClass){
                var v = selectedClass - 3;

                if(v<-1){
                    v += 6;
                }

                var eleLeft = Math.round((parseFloat($(this).css("left")) / document.documentElement.clientHeight ) * 100) + 20;


                if(selectedClass==0)
                    $(this).animate({top:'+100vh',left:'+'+eleLeft+'vh'},600);
                if(selectedClass==1)
                    $(this).animate({top:'+100vh',left:'-'+eleLeft+'vh'},600);

                if(selectedClass==5)
                    $(this).animate({left:'+200vh'},600);

                if(selectedClass==2)
                    $(this).animate({left:'-200vh'},600);

                if(selectedClass==3)
                    $(this).animate({top:'-200vh',left:'-'+eleLeft+'vh'},600);
                if(selectedClass==4)
                    $(this).animate({top:'-200vh',left:'+'+eleLeft+'vh'},600);

                $(this).addClass("delete");
            }
        });
        setTimeout(function(){
            $(".delete").remove();
        },1000);

        //Main Component Detail
        var mainComponent = $("#main-component");
        var mainCompId = mainComponent.attr("title");
        var mainCompHeadline = mainComponent.children("div").children(".news-content").children("strong").children("p").text();


        position = find_position(search(selectedCompId),mainCompId);

        //  alert(position);

        var mainTop = Math.round((parseFloat(mainComponent.css("top")) / document.documentElement.clientHeight ) * 100);
        var considerTop = parseInt(shifting[position].top) - mainTop;
        var mainLeft = Math.round((parseFloat(mainComponent.css("left")) / document.documentElement.clientHeight ) * 100);
        var considerLeft = parseInt(shifting[position].left) - mainLeft;

        var topVal = "+"+(considerTop+mainTop)+"vh";
        if((considerLeft+mainLeft)<0){
            var leftVal = (considerLeft+mainLeft)+"vh";
        }
        else{
            var leftVal = "+"+(considerLeft+mainLeft)+"vh";
        }

        var back_image = mainComponent.children(".main-photo").attr("style");


        mainComponent.empty();
        mainComponent.addClass("component");
        mainComponent.animate({
            width: '200px',
            height: '120px',
            borderRadius: '100px',
            cursor: 'pointer',
            top : topVal,
            left : leftVal,
        },1000);
        div_open = "<div class='component-photo' style='"+back_image+"'></div>";
        var maxLength = 20;
        var name = mainCompHeadline;
        if(name.length >= maxLength){
            name = name.substr(0,maxLength-2) + "...";
        }
        var child = "\t<div class='component-headline'><h4>" + name[0].toUpperCase() + name.substr(1,name.length+1) + "</h4></div>\n";
        var div_close = "</div>";
        var div = div_open + child + div_close;
        mainComponent.append(div);
        container.append(mainComponent);
        mainComponent.addClass("component-"+position);
        mainComponent.removeAttr("id");

        selectedComponent.empty();
        //selectedComponent.removeClass("component");
        $(".graph").prepend(selectedComponent);
        selectedComponent.animate({
            width: '43%',
            margin : '20vh 25vh',
            borderRadius: '15px',
            height: '350',
            top: 0,
            left: 0,
        },1000);


        setTimeout(function(){
            selectedComponent.attr("id","main-component");
            creating_main_component(selectedElement);
        },1000);

        var newComponents = search(selectedCompId)["related_event_ids"];
        
        $.ajax({
            url : '/struct-events/'+selectedCompId,  // http://127.0.0.1/
            method : 'GET',
            success: function(response_data){
                api_data = response_data;
            },
            error: function(e){
                alert("URl is not Working....");
            }
        });
            
        

        setTimeout(function(){
            var components = [];
            newComponents.forEach(element => {
                components.push(search(element));
            });            

            setTimeout(function(){
                addComponent(api_data["struct-events"]["main"]["related_event_ids"], position);
            },700);
        },500);

    });
});