
# reference to trending event ID
TRENDING_EVENT_ID = None

import hashlib

def trim_kwargs(**kwargs):
    ''' To remove unnecessary (None valued) keyword argument and trim '''

    args = dict()
    for key, val in kwargs.items():
        if kwargs[key]: args[key] = val
    return args

def get_hash(data):
    ''' return hash of given data
        Chaning code here will require all previous ids needs to be updated
    '''

    data = str(data).encode()
    return hashlib.md5(data).hexdigest()

def filter_events(events):
    ''' it will change naming convention of events '''
    return [filter_event(event) for event in events]

def filter_event(event):
    ''' it will change naming convention of event '''

    return {
        '_id': event.get('_id'),
        'name': event.get('name'),
        'title': event.get('title'),
        'published_at': event.get('publishedAt'),
        'description': event.get('description'),
        'content': event.get('content'),
        'author': event.get('author'),
        'url': event.get('url'),
        'image_url': event.get('urlToImage'),
        'source': event.get('source'),
        'keywords': event.get('keywords'),
        'related_event_ids': event.get('related_event_ids'),
    }