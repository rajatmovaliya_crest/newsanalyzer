from gensim.summarization import keywords
from collections import OrderedDict
import nltk

KEYWORDS_LIMIT = 10

def extract_keywords_from_event(event, limit=KEYWORDS_LIMIT):
    ''' return extracted keywords of given event based on specific parameters '''

    try:
        ip = [
            event.get('title', ''),
            event.get('description', ''),
            event.get('content'),
        ]
        return extract_keywords_through_nltk(ip)[:limit]
    except Exception as ex:
        pass
        #print(f'{ex}')
        return []

def extract_keywords_through_gensim(texts, limit=KEYWORDS_LIMIT):
    '''
        Params:
            - texts (list): list of text from which we want to extract the keywords
            - limit (int): max no. of keywords that user wants
                - defualt = 20
        Return:
            - list of keywords
    '''
    generated_keywords = []
    for text in texts:
        tags = keywords(text, ratio=1, pos_filter=('NN','JJ'), lemmatize=True).split("\n")
        if tags not in [[''] ,[]]:
            generated_keywords.extend(tags)
            if len(generated_keywords) > limit:
                break
    return list(OrderedDict.fromkeys(generated_keywords))[:limit]

def extract_keywords_through_nltk(texts, limit=KEYWORDS_LIMIT):
    '''
        Params:
            - texts (list): list of text from which we want to extract the keywords 
            - limit (int): max no. of keywords that user wants
                - defualt = 20
        Return:
            - list of keywords
    '''
    keywords = []
    text_data = ''
    for text in texts:
        text_data = text_data + text
    '''
        * word_tokenize : seperates the unstructured text into tokens
        * pos_tag : assigns part of speech tags to tokens and returns tuples
        * ne_chunk : returns 2 level tree structure of named entities
            - NE types :
                ORGANIZATION
                PERSON
                LOCATION
                DATE
                TIME
                MONEY
                FACILITY
                GPE
    '''
    chunks = nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(text_data)))
    # chunks.draw()
    for chunk in chunks:
        if hasattr(chunk, 'label'):
            keys = str(' '.join(c[0] for c in chunk.leaves()))
            if keys.replace(' ','') in keywords:
                keywords.remove(keys.replace(' ',''))
            keywords.append(str(' '.join(c[0] for c in chunk.leaves()).lower()))
    if len(keywords) < limit:
        keywords.extend(extract_keywords_through_gensim(texts, limit - len(keywords)))
    return list(OrderedDict.fromkeys(keywords))

if __name__ == "__main__":

    text_list = [
        'JNU Violence LIVE Updates: JNUSU chief Aishe Ghosh booked for vandalism, but Delhi Police fails to nab masked men who attacked her - Firstpost',
        'JNU Violence Latest Updates: The four-member fact-finding committee constituted by the Congress following the violence reported at JNU on Sunday, will meet interim party chief Sonia Gandhi at 4.30 pm. The committee is also likely to start meeting students',
    ]
    print(extract_keywords_through_nltk(text_list, 10))