import time
import os
import sys

sys.path.append(os.path.join(os.path.abspath(__file__), os.pardir))

import news_api as na
import analytics
import db
import utils

from concurrent.futures import ThreadPoolExecutor
from threading import Thread, Lock
from queue import Queue

# Note: Don't change constant's order, here order matters

# number of related events that will be kept with main events
NUM_RELATED_EVENTS = 6

# Daemon threads will try to fetch all events by going recursively upto given level
UPTO_LEVEL = 3

# Number of concurrent threads that will process fetchin neighbours of given event id
CONCURRENT = 3 #NUM_RELATED_EVENTS * (NUM_RELATED_EVENTS-1)

# Frequency by which trending event will be updated
UPDATE_FREQ = 2 * 24 * 60 * 60    # in seconds

# NewsApi Delay before every request
NEWSAPI_DELAY = 2

# Thread Lock
lock = Lock()

'''
Queue from which daemon threads will get ids if any and process

Queue Element Format:
"(event_id, upto_level)" or ("event_id",)

event_id: event id from which Executer will start fetching and storing it's neighbour events
upto_level: Executer will try to fetch and store events upto this levels
'''

QUEUE_SIZE = CONCURRENT * 50
ids_queue = Queue(QUEUE_SIZE)

db = db.DataBase()
news_api = na.NewsAPI()

def start_bgs():
    ''' Start BackGroundServices  '''

    bgs_trending_news = Thread(target=update_trending_news)
    bgs_trending_news.daemon = True
    bgs_trending_news.start()
    for _ in range(CONCURRENT):
        t = Thread(target=update_nbrs)
        t.daemon = True
        t.start()


def update_trending_news():
    ''' Update trending event from newsapi '''

    global ids_queue
    utils.TRENDING_EVENT_ID = db.get_trending_event_id()

    while True:

        trending_news = []
        trending_news = news_api.get_trending_news()

        if trending_news in [[], None]:
            time.sleep(UPDATE_FREQ)
            continue

        trending_news = trending_news[0]
        trending_news['_id'] = news_api.get_news_id(trending_news)

        if db.is_event_exists(_id=trending_news['_id']):
            db.insert_trending_event_id(_id=trending_news['_id'])
            utils.TRENDING_EVENT_ID = db.get_trending_event_id()
            time.sleep(UPDATE_FREQ)
            continue

        keywords = analytics.extract_keywords_from_event(trending_news)
        keywords = [''] if keywords in [None, []] else keywords
        trending_news['name'] = keywords[0]

        db.insert_trending_event_id(_id=trending_news['_id'])
        db.insert_event(trending_news)
        utils.TRENDING_EVENT_ID = db.get_trending_event_id()

        ids_queue.put((trending_news['_id'],))  # put on query for further neighbour events processing
        time.sleep(UPDATE_FREQ)

def handle_child_event(keyword, event, upto_level):
    ''' Thread function '''

    global ids_queue

    if len(event['related_event_ids']) >= NUM_RELATED_EVENTS: return

    time.sleep(NEWSAPI_DELAY)
    news = news_api.get_news(query=keyword)
    if not (news and isinstance(news, list)): return

    news = news[0]
    news['name'] = keyword
    news['_id'] = news_api.get_news_id(news)
    news['related_event_ids'] = [event['_id'],]   # add parent node to child node

    if len(event['related_event_ids']) >= NUM_RELATED_EVENTS: return
    event['related_event_ids'].append(news['_id'])   # add child node to parent

    if keyword in event['keywords']: return
    event['keywords'].append(keyword)

    db.insert_event(news)
    db.replace_event(event)

    # start processing neighbours of this news
    if upto_level >= 2:
        ids_queue.put((news['_id'], upto_level-1))

def update_nbrs():
    ''' BackGroundService for updating neighbour events of given event from queue '''

    global ids_queue

    while True:
        upto_level = UPTO_LEVEL

        # take all forms of argument from queue
        args = ids_queue.get()

        if not (args and isinstance(args, (tuple, list))):
            continue
        elif len(args) == 1:
            (_id,) = args
        elif len(args) == 2:
            (_id, upto_level) = args
        else:
            (_id, upto_level, *_) = args

        if upto_level <= 0 :
            continue

        event = db.get_event(_id=_id)
        if event is None:
            continue

        if not event.get('related_event_ids'): event['related_event_ids'] = []

        remaing_count = NUM_RELATED_EVENTS - len(event['related_event_ids'])
        remaing_count = 0 if remaing_count < 0 else remaing_count

        if remaing_count <= 0:

            # Don't include last layer in queue to avoid last layer news processing
            if upto_level >= 2:
                # skipping first related event ids as it will be parent itself
                for _id in event['related_event_ids'][1:]:
                    ids_queue.put((_id, upto_level-1))
            continue

        if not event.get('keywords'): event['keywords'] = []
        keywords = analytics.extract_keywords_from_event(event, limit=10)

        num_less_keywords = len(keywords) - NUM_RELATED_EVENTS
        if num_less_keywords > 0:
            keywords.extend(na.TRENDING_KEYWORDS[:num_less_keywords])

        params = [(keyword, event, upto_level-1) for keyword in keywords]
        with ThreadPoolExecutor(max_workers=1) as exe:
            exe.map(lambda p: handle_child_event(*p), params)

        ids_queue.task_done()