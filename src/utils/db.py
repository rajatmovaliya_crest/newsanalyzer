import pymongo
import os
import sys
import json

sys.path.append(os.path.join(os.path.abspath(__file__), os.path.pardir))

import utils

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'crestds'
MONGODB_COLLECTION = 'newsanalyzer'

CACHE_DIR_PATH = os.path.join(os.path.abspath(__file__), os.path.pardir, os.path.pardir, os.path.pardir, '.cache')
CACHE_FILE_PATH = os.path.join(CACHE_DIR_PATH, 'cache.json')
os.makedirs(CACHE_DIR_PATH, exist_ok=True)

LIMIT = 10

class DataBase:

    def __init__(self, host=MONGODB_HOST, port=MONGODB_PORT, db=MONGODB_DB, collection=MONGODB_COLLECTION):
        self.client = pymongo.MongoClient(host, port)
        self.db = self.client[db]
        self.collection = self.db[collection]

    def insert_trending_event_id(self, _id=None):
        ''' insert trending id into database '''

        try:
            with open(CACHE_FILE_PATH, 'w') as fp:
                json.dump({'trending_event_id': _id}, fp)
        except Exception as ex:
            print(f'{ex}')
            return False
        return True

    def get_trending_event_id(self):
        ''' return trending event id '''

        data = {}
        try:
            with open(CACHE_FILE_PATH, 'r') as fp:
                data = json.load(fp)
        except Exception as ex:
            print(f'{ex}')
            return None

        return data.get('trending_event_id', None)

    def get_trending_struct_events(self):
        return self.get_struct_events(_id=utils.TRENDING_EVENT_ID)

    def is_event_exists(self, _id=None, name=None):
        ''' check if given event exists in db '''

        if _id:
            return self.get_event(_id=_id) is not None
        elif name:
            return self.get_event(name=name) is not None
        else:
            return False

    def replace_event(self, event):
        ''' Replace given event based on ID '''

        res = None
        event = utils.filter_event(event)
        try:
            res = self.collection.replace_one({'_id': event['_id']}, event)
        except Exception as ex:
            print(f'{ex}')

        return res

    def insert_events(self, events):
        ''' Insert events '''

        res = None
        events = utils.filter_events(events)

        try:
            res = self.collection.insert_many(events)
        except pymongo.errors.DuplicateKeyError as ex:
            pass

        return res

    def insert_event(self, event):
        ''' Insert event '''

        res = None
        event = utils.filter_event(event)
        try:
            res = self.collection.insert_one(event)
        except pymongo.errors.DuplicateKeyError as ex:
            pass

        return res

    def get_event(self, _id=None, name=None):
        ''' Return event with filters applied '''

        search_filter = dict()
        if _id: search_filter['_id'] = _id
        if name: search_filter['name'] = name
        data = self.collection.find_one(search_filter)
        if data and isinstance(data, dict):
            return data
        else:
            return None

    def get_events(self, ids=None, query=None, names=None, limit=LIMIT):
        ''' Return events with filters applied '''

        data = None
        search_filter = dict()
        if ids: search_filter['_id'] = {'$in': ids,}
        if names: search_filter['name'] = {'$in': names,}
        if query:
            search_filter['name'] = { '$regex': f'.*{query}.*'}

        data = self.collection.find(search_filter).limit(limit)
        if data:
            return list(data)
        else:
            return None

    def get_struct_events(self, _id=None, name=None, limit=LIMIT):
        ''' Return structured event '''

        main_event = None
        if _id:
            main_event = self.get_event(_id=_id)
        elif name:
            main_event = self.get_event(name=name)
        else:
            return (None, None)

        if not main_event: return (None, None)
        related_events = self.get_events(ids=main_event['related_event_ids'])

        return  (main_event, related_events)