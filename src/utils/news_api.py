'''
Easy to use NewsAPI Interface
'''

import os
import sys
import time
import random

sys.path.append(os.path.join(os.path.abspath(__file__), os.path.pardir))

import analytics
import db

from utils import trim_kwargs
from utils import get_hash
from newsapi import NewsApiClient
from newsapi.newsapi_exception import NewsAPIException

TRENDING_KEYWORDS = []

API_KEYS = [
    'f6f7e889558c401e9a4534b4c866a664',
    '638c52963cb64a3b80a01213c0c1c439',
    '907cf3a20cc8446080ce41830641a2f3',
    'e5a4e89bf4e4435588fafb8b9e36c652',
    '9027e8913d104870a08ba74ed59e293f',
    'dbb59e8a286a4d6a8425a15bae1c8eb1',
    '24043bb4894248fe91eeb123916c5b39',
    '0dfaad71cc464ec1a39b7dcc304939f9',
    '88fc4859a4f046d8b01e1f4e5fa77158',
    'd70ee13db88e40d5b439a81deb077040',
    'b9b0d4be0fa64df8b4a7d50b5278bf22',
    'c77489d4764947d89ef1d5c539ab3432',
    '85bc644c5d6c4699b5bbc9374019774b',
    '75286d563cb04aa8a65c11b457204451',
    'b544e1fc80904fcbaf629198dda34e93',
    'c5864a134d4841868c635dce50661c4b',
    'e848e34f9f7f49bbbd4a456602e5e1f8',
    'fd41f52898c940f892918a6e152baf36',
    'cc7163c6e1b947d38dbc6d58d35ecdff',
    '5f0f6b9e6f7b49ebacb444c5ad83b64c',
    'd08b3fef02b04b62ade752144655a70a',
    '497d4365100c4eff8b088b2f5fe6e968',
    '4dbc17e007ab436fb66416009dfb59a8',
    '9d3b62c945f2475d8e2d395a04e82107',
    '838b62c7059448b0ad8383231c8ac614',
    'e72bb370d548488c9919ed7f61aa6346',
    '967bf532b2894942b4a2d0f12aabdea5',
    '83131fddc9544a5384a6493190036849',
    'c6da391e3dbc4205ba1c959b24fb88e4',
    '94fe52ff28a14b8aa636f5595b38dd8f',
    '1ef09b50f20e4c9d8afe0c6f19287418',
    '97e0befa4c9e419fbe5c4f3d39942d80',
    '4fb61e8d2a724278b488b52d21d92186',
    '88bbf24043fb4293b9068482dbb21f12',
    '2c8ceafd6b904b8294c1017c6910fd3d',
    'a402ae2289b649e3b57e2b1863ef55ce',
    '004c99be32c448e38ec95b26fc75156c',
]

class NewsAPI:

    def __init__(self):
        self.api_key_index = 0
        self.newsapi = NewsApiClient(api_key=API_KEYS[self.api_key_index])
        self.db = db.DataBase()
        self.api_req_count = 0

    def rotate_api_key(self):
        ''' change the api key of newsapi '''

        print('Rotating API key ...')
        if self.api_key_index >= len(API_KEYS) - 1:
            print('All API Keys exhausted')
            self.api_key_index = 0
            return False
        else:
            self.api_key_index += 1
            self.newsapi = NewsApiClient(api_key=API_KEYS[self.api_key_index])
            return True

    def get_news_id(self, news):
        ''' Return new id of given news '''

        if not(news and isinstance(news, dict)): return None
        ip = news.get('title', '') + news.get('description', '')
        return get_hash(ip)

    def get_news(self, query, sort_by='relevancy', _from=None, to=None, page=20, language='en'):
        ''' Return news related to given query
            It returns standard news structure as defined in this module's doc string
        '''

        kwargs = {
            'q': query,
            'from_param': _from,
            'to': to,
            'sort_by': sort_by,
            # 'page': page,     # skipping it as it is not working for 3rd party module "newsapi"
            'language': language
        }

        data = {'articles': [], 'status': 'ok'}
        try:
            self.api_req_count += 1
            print('Everything NewsAPI Request, Total Count: ', self.api_req_count)
            data = self.newsapi.get_everything(**trim_kwargs(**kwargs))
            if not data.get('articles', None): raise NewsAPIException
        except NewsAPIException:
            if self.rotate_api_key():
                data['articles'] = self.get_news(query, sort_by, _from, to, page, language)
            else:
                return []

        if isinstance(data['articles'], list) and len(data['articles']) > 1:
            random.shuffle(data['articles'])

        return data['articles'][:page]

    def get_trending_news(self, query=None, language='en', country='in', page=1):
        ''' Return trending news '''

        global TRENDING_KEYWORDS
        kwargs = {
            'q': query,
            'language': language,
            'country': country,
            #'page': page
        }

        data = {'articles': [], 'status': 'ok'}
        try:
            self.api_req_count += 1
            print('Trending NewsAPI Request, Total Request Count: ', self.api_req_count)
            data = self.newsapi.get_top_headlines(**trim_kwargs(**kwargs))
            if not data.get('articles', None): raise NewsAPIException
        except NewsAPIException:
            if self.rotate_api_key():
                data['articles'] = self.get_trending_news(query, language, country, page)
            else:
                return []

        TRENDING_KEYWORDS = []
        for article in data['articles']:
            TRENDING_KEYWORDS.append(analytics.extract_keywords_from_event(article)[:1])


        return data['articles'][:page]