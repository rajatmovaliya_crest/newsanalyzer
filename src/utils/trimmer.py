
def trim_dicts(events, keys):
    ''' apply "trimmer_dict" function on list of dicts '''
    return [trim_dict(event, keys) for event in events]

def trim_dict(event, keys):
    ''' Only allow given keys in object '''
    out = {}
    for key in event.keys():
        if key not in keys: continue
        out[key] = event[key]
    return out

if __name__ == "__main__":
    print(trim_dict({'1':1, '2':2, '3': 3}, ['1', '2']))