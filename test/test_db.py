import os
import sys

sys.path.append( os.path.join( os.path.dirname(__file__), os.path.pardir))
from src.utils.db import DataBase

EVENT = {
    '_id': 1,
    'name': 'modi',
    'related_ids': [2, 3]
}

db = DataBase(db='test_db', collection='test')
db.insert_event(EVENT)

def test_event_not_found():
    assert db.get_event(_id=2) == None

def test_event_found():
    assert db.get_event(_id=1) == EVENT